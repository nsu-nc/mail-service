package ru.nsu.ccfit.nsunc.mail.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import java.util.List;

@Entity
@Data
@EnableAutoConfiguration
@Table(name = "events")
public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column
    private String name;
    @Column
    private Boolean subAbility;
    @Column
    private Boolean deleted;
    @OneToMany(mappedBy = "event")
    private List<History> histories;
    @ManyToMany(fetch = FetchType.EAGER)
    private List<Subscriber> subscribers;
}
