package ru.nsu.ccfit.nsunc.mail.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import ru.nsu.ccfit.nsunc.mail.dto.NotifyInfoDto;
import ru.nsu.ccfit.nsunc.mail.dto.RecipientDto;

@Service
public class MailServiceImpl implements MailService {
    private final JavaMailSender mailSender;
    @Value("${spring.mail.username}") private String sender;

    @Autowired
    public MailServiceImpl(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    @Override
    public void notifyEvent(NotifyInfoDto notifyInfoDto) {
        for (RecipientDto recipient : notifyInfoDto.getRecipients()) {
            SimpleMailMessage mailMessage
                    = new SimpleMailMessage();

            mailMessage.setFrom(sender);
            mailMessage.setTo(recipient.getEmail());
            mailMessage.setText(notifyInfoDto.getText());
            mailMessage.setSubject(notifyInfoDto.getSubject());

            mailSender.send(mailMessage);
        }
    }
}
