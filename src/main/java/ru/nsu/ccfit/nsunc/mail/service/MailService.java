package ru.nsu.ccfit.nsunc.mail.service;

import ru.nsu.ccfit.nsunc.mail.dto.NotifyInfoDto;

public interface MailService {
    void notifyEvent(NotifyInfoDto notifyInfoDto);
}
