package ru.nsu.ccfit.nsunc.mail.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class NotifyInfoDto {
    private String eventName;
    private List<RecipientDto> recipients;
    private String subject;
    private String text;
}
