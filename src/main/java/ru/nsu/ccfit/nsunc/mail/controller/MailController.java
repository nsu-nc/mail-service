package ru.nsu.ccfit.nsunc.mail.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.ccfit.nsunc.mail.dto.NotifyInfoDto;
import ru.nsu.ccfit.nsunc.mail.service.MailService;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
@Tag(name = "Mail Api:)")
public class MailController {
    private final MailService mailService;

    @Autowired
    public MailController(MailService mailService) {
        this.mailService = mailService;
    }

    @Operation(
            operationId = "notifyEvent",
            summary = "Notify event to make a newsletter",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Successfully event notified"),
                    @ApiResponse(responseCode = "500", description = "Internal Server Error")
            }
    )
    @PostMapping(value = "/event/new", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> notifyEvent(@Parameter(name = "NotifyInfoDto",
            description = "Information about event and recipients", required = true)
                                              @RequestBody NotifyInfoDto notifyInfoDto) {
        mailService.notifyEvent(notifyInfoDto);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
