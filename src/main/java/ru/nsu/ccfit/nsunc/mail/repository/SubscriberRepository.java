package ru.nsu.ccfit.nsunc.mail.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.ccfit.nsunc.mail.entity.Subscriber;

public interface SubscriberRepository extends JpaRepository<Subscriber, Long> {
}
